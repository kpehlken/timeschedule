import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import VueTouch from 'vue-touch';

VueTouch.registerCustomEvent('doubletap', {
    type: 'tap',
    taps: 2,
});

createApp(App).use(store).use(router).use(VueTouch, { name: 'vue-touch' }).mount('#app')

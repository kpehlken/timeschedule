import { createStore } from 'vuex'

export default createStore({
  state: {
    schedule: {},
    score: localStorage.getItem("score") | 1,
    weekend: null,                    // only possible values: m, d
    week: localStorage.getItem("week") // only possible values: a,b
  },
  mutations: {
      updateScore(state, amount) {
          state.score += amount;
          localStorage.setItem("score", state.score);
      },
      setSchedule(state, todaysSchedule) {
          state.schedule = todaysSchedule;
      },
      setWeekend(state, weekendData) {
        state.weekend = weekendData;
      }
  },
  actions: {
  },
  modules: {
  }
})

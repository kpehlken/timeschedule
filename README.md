# timeschedule

## Personal Project for Time Management
Only intended to be used by kpehlken

## Tech Stack
### Vue.js - Frontend Framework
### Capacitor.js - Cross-Platform Wrapper for Android, IOS and Desktop Compatibility

## Time Methods

mounted (App.vue) - Start of app e.g. load schedule check week 
updateSchedule (Schedule.vue) - Handle styling of passed tasks


## Week Handling
Specify week (a or b) in url of "today/schedule/?week"


## Edge Cases

Holidays - Save Holiday Schedule and fetch in on manual request
Trips


## Backend Parts needed

/schedule/holiday - Return [ { _id, week, day, schedule: { {}, {}, {}, {} } } ] 
/today/schedule/?week -  Return Schedule depending on specified Week
/today/schedule/weekend/?weekend - Return Schedule depending on specifier (m or d) [ { _id, week, day, schedule: { {}, {}, {}, {} } } ] 

## TODO

WeekendCheck.vue - v-if line 3 remove !
Events.vue - Holiday Button
Error.vue - Style, functionality
Categories.vue - Entire
Score.vue - Entire
App.vue - Reduce Touch Sensitivity
Schedule.vue - Prevent completing future tasks 

## Schedules

B-Week Schedules (Weekend Schedules) 
Holiday Schedule
